/**
*
*
* @param {Object} params
* @returns {Promise.<Response>} Response promise
*
*
*/

module.exports = function ({
  name,
  description,
  product,
  protocol,
  devicePoolName,
  securityProfileName,
  sipProfileName,
  lines,
  phoneTemplateName
}) {
  const getSoapBody = pattern => {

    let linesString = '<lines>'

    for (let i = 0; i < lines.length; i++) {
      let endUsersString = ''
      for (let j = 0; j < lines[i].users.length; j++) {
        endUsersString += '<enduser>' +
        '<userId>' + lines[i].endUsers[j].userId + '</userId>' +
        '</enduser>'
      }

      linesString += '<line>' +
      '<index>' + (i + 1) + '</index>' +
      '<dirn uuid="' + lines[i].uuid + '"></dirn>' +
      '<associatedEndusers>' +
      endUsersString +
      '</associatedEndusers>' +
      '</line>' +
    }
    linesString += '</lines>'


    return this.getSoapEnv().replace(
      '{{body}}',
      '<ns:addPhone sequence="?">' +
      '<phone ctiid="?">' +
      '<name>' + name + '</name>' +
      '<description>' + description + '</description>' +
      '<product>' + product + '</product>' +
      '<class>Phone</class>' +
      '<protocol>' + protocol + '</protocol>' +
      '<protocolSide>User</protocolSide>' +
      '<devicePoolName>' + devicePoolName + '</devicePoolName>' +
      '<securityProfileName>' + securityProfileName + '</securityProfileName>' +
      '<sipProfileName>' + sipProfileName + '</sipProfileName>' +
      linesString +
      '<phoneTemplateName>' + phoneTemplateName + '</phoneTemplateName>' +
      '</phone>' +
      '</ns:addPhone>' +
    )
  }

  // TODO: Error handling at this level

  return Promise.all([
    getSoapBody(pattern),
    this.constructHeaders('addPhone')
  ])
  .then(this.callApi)
  .then(this.parseResult)
  .then(this.convertXml)
  .then(this.trimJsonBody);
}
