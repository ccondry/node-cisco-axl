/**
*
*
* @param {any} number
* @returns {Promise.<Response>} Response promise
*
*
*/
module.exports = function ({number, uuid}) {
  const getSoapBody = pattern => {
    let id
    if (number) {
      id = '<pattern>' + number + '</pattern>'
    } else {
      id = '<uuid>' + uuid + '</uuid>'
    }
    return this.getSoapEnv().replace(
      '{{body}}',
      '<ns:updateLine sequence="?">' +
      id +
      '<newPattern>?</newPattern>' +
      '<description>?</description>' +
      '<newRoutePartitionName uuid="?">?</newRoutePartitionName>' +
      '<aarNeighborhoodName uuid="?">?</aarNeighborhoodName>' +
      '<aarDestinationMask>?</aarDestinationMask>' +
      '<aarKeepCallHistory>?</aarKeepCallHistory>' +
      '<aarVoiceMailEnabled>?</aarVoiceMailEnabled>' +
      '<callForwardAll>' +
      '<forwardToVoiceMail>?</forwardToVoiceMail>' +
      '<callingSearchSpaceName uuid="?">?</callingSearchSpaceName>' +
      '<secondaryCallingSearchSpaceName uuid="?">?</secondaryCallingSearchSpaceName>' +
      '<destination>?</destination>' +
      '</callForwardAll>' +
      '<callForwardBusy>' +
      '<forwardToVoiceMail>?</forwardToVoiceMail>' +
      '<callingSearchSpaceName uuid="?">?</callingSearchSpaceName>' +
      '<destination>?</destination>' +
      '</callForwardBusy>' +
      '<callForwardBusyInt>' +
      '<forwardToVoiceMail>?</forwardToVoiceMail>' +
      '<callingSearchSpaceName uuid="?">?</callingSearchSpaceName>' +
      '<destination>?</destination>' +
      '</callForwardBusyInt>' +
      '<callForwardNoAnswer>' +
      '<forwardToVoiceMail>?</forwardToVoiceMail>' +
      '<callingSearchSpaceName uuid="?">?</callingSearchSpaceName>' +
      '<destination>?</destination>' +
      '<duration>?</duration>' +
      '</callForwardNoAnswer>' +
      '<callForwardNoAnswerInt>' +
      '<forwardToVoiceMail>?</forwardToVoiceMail>' +
      '<callingSearchSpaceName uuid="?">?</callingSearchSpaceName>' +
      '<destination>?</destination>' +
      '<duration>?</duration>' +
      '</callForwardNoAnswerInt>' +
      '<callForwardNoCoverage>' +
      '<forwardToVoiceMail>?</forwardToVoiceMail>' +
      '<callingSearchSpaceName uuid="?">?</callingSearchSpaceName>' +
      '<destination>?</destination>' +
      '</callForwardNoCoverage>' +
      '<callForwardNoCoverageInt>' +
      '<forwardToVoiceMail>?</forwardToVoiceMail>' +
      '<callingSearchSpaceName uuid="?">?</callingSearchSpaceName>' +
      '<destination>?</destination>' +
      '</callForwardNoCoverageInt>' +
      '<callForwardOnFailure>' +
      '<forwardToVoiceMail>?</forwardToVoiceMail>' +
      '<callingSearchSpaceName uuid="?">?</callingSearchSpaceName>' +
      '<destination>?</destination>' +
      '</callForwardOnFailure>' +
      '<callForwardAlternateParty>' +
      '<callingSearchSpaceName uuid="?">?</callingSearchSpaceName>' +
      '<destination>?</destination>' +
      '<duration>?</duration>' +
      '</callForwardAlternateParty>' +
      '<callForwardNotRegistered>' +
      '<forwardToVoiceMail>?</forwardToVoiceMail>' +
      '<callingSearchSpaceName uuid="?">?</callingSearchSpaceName>' +
      '<destination>?</destination>' +
      '</callForwardNotRegistered>' +
      '<callForwardNotRegisteredInt>' +
      '<forwardToVoiceMail>?</forwardToVoiceMail>' +
      '<callingSearchSpaceName uuid="?">?</callingSearchSpaceName>' +
      '<destination>?</destination>' +
      '</callForwardNotRegisteredInt>' +
      '<callPickupGroupName uuid="?">?</callPickupGroupName>' +
      '<autoAnswer>Auto Answer Off</autoAnswer>' +
      '<networkHoldMohAudioSourceId>?</networkHoldMohAudioSourceId>' +
      '<userHoldMohAudioSourceId>?</userHoldMohAudioSourceId>' +
      '<alertingName>?</alertingName>' +
      '<asciiAlertingName>?</asciiAlertingName>' +
      '<presenceGroupName uuid="?">?</presenceGroupName>' +
      '<shareLineAppearanceCssName uuid="?">?</shareLineAppearanceCssName>' +
      '<voiceMailProfileName uuid="?">?</voiceMailProfileName>' +
      '<patternPrecedence>Default</patternPrecedence>' +
      '<releaseClause>No Error</releaseClause>' +
      '<hrDuration>?</hrDuration>' +
      '<hrInterval>?</hrInterval>' +
      '<cfaCssPolicy>Use System Default</cfaCssPolicy>' +
      '<defaultActivatedDeviceName uuid="?">?</defaultActivatedDeviceName>' +
      '<parkMonForwardNoRetrieveDn>?</parkMonForwardNoRetrieveDn>' +
      '<parkMonForwardNoRetrieveIntDn>?</parkMonForwardNoRetrieveIntDn>' +
      '<parkMonForwardNoRetrieveVmEnabled>?</parkMonForwardNoRetrieveVmEnabled>' +
      '<parkMonForwardNoRetrieveIntVmEnabled>?</parkMonForwardNoRetrieveIntVmEnabled>' +
      '<parkMonForwardNoRetrieveCssName uuid="?">?</parkMonForwardNoRetrieveCssName>' +
      '<parkMonForwardNoRetrieveIntCssName uuid="?">?</parkMonForwardNoRetrieveIntCssName>' +
      '<parkMonReversionTimer>?</parkMonReversionTimer>' +
      '<partyEntranceTone>Default</partyEntranceTone>' +
      '<directoryURIs>' +
      '<directoryUri>' +
      '<isPrimary>?</isPrimary>' +
      '<uri>?</uri>' +
      '<partition uuid="?">?</partition>' +
      '<advertiseGloballyViaIls>true</advertiseGloballyViaIls>' +
      '</directoryUri>' +
      '</directoryURIs>' +
      '<allowCtiControlFlag>true</allowCtiControlFlag>' +
      '<rejectAnonymousCall>?</rejectAnonymousCall>' +
      '<patternUrgency>false</patternUrgency>' +
      '<confidentialAccess>' +
      '<confidentialAccessMode>?</confidentialAccessMode>' +
      '<confidentialAccessLevel>?</confidentialAccessLevel>' +
      '</confidentialAccess>' +
      '<externalCallControlProfile uuid="?">?</externalCallControlProfile>' +
      '<enterpriseAltNum>' +
      '<numMask>?</numMask>' +
      '<isUrgent>false</isUrgent>' +
      '<addLocalRoutePartition>true</addLocalRoutePartition>' +
      '<routePartition uuid="?">?</routePartition>' +
      '<advertiseGloballyIls>true</advertiseGloballyIls>' +
      '</enterpriseAltNum>' +
      '<e164AltNum>' +
      '<numMask>?</numMask>' +
      '<isUrgent>false</isUrgent>' +
      '<addLocalRoutePartition>true</addLocalRoutePartition>' +
      '<routePartition uuid="?">?</routePartition>' +
      '<advertiseGloballyIls>true</advertiseGloballyIls>' +
      '</e164AltNum>' +
      '<pstnFailover>?</pstnFailover>' +
      '<callControlAgentProfile>?</callControlAgentProfile>' +
      '<useEnterpriseAltNum>?</useEnterpriseAltNum>' +
      '<useE164AltNum>?</useE164AltNum>' +
      '<active>true</active>' +
      '</ns:updateLine>' +
    )
  }

  // TODO: Error handling at this level

  return Promise.all([
    getSoapBody(pattern),
    this.constructHeaders('updateLine')
  ])
  .then(this.callApi)
  .then(tihs.parseResult)
  .then(this.xmltoJSON)
  .then(this.trimJsonBody);
}
