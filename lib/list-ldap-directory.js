
/**
*
*
* @returns {Promise.<Response>} Response promise
*
*
*/
function listLdapDirectory() {
  const getSoapBody = () => new Buffer(
    this.getSoapEnv().replace(
      '{{body}}',
      '<ns:listLdapDirectory sequence="?">' +
      '<searchCriteria>' +
      '<name>%</name>' +
      '</searchCriteria>' +
      '<returnedTags uuid="?">' +
      '<name>?</name>' +
      '<ldapDn>?</ldapDn>' +
      '<userSearchBase>?</userSearchBase>' +
      '</returnedTags>' +
      '</ns:listLdapDirectory>'
    )
  );

  const trimJson = json => this.trimJson(
    json, 'listLdapDirectory', 'ldapDirectory'
  )

  return Promise.all([
    getSoapBody(),
    this.constructHeaders('listLdapDirectory')
  ])
  .then(this.callApi)
  .then(this.parseResult)
  .then(this.convertXml)
  .then(trimJson);
}

module.exports = listLdapDirectory
