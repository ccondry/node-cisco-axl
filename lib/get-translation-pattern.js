/**
*
*
* @param {any} query
* @returns {Promise.<Response>} Response promise
*
*
*/
module.exports = function (query) {
  const getSoapBody = (query) => new Buffer(
    this.getSoapEnv().replace(
      '{{body}}',
      '<ns:getTransPattern sequence="?">' +
      '<'+query.type+'>' + query.search + '</'+query.type+'>' +
      '<routePartitionName uuid="?">' + (query.partition || '') + '</routePartitionName>' +
      '<returnedTags uuid="?">' +
      '<pattern>?</pattern>' +
      '<description>?</description>' +
      '<routePartitionName uuid="?">?</routePartitionName>' +
      '<calledPartyTransformationMask>?</calledPartyTransformationMask>' +
      '<callingPartyTransformationMask>?</callingPartyTransformationMask>' +
      '<useCallingPartyPhoneMask>?</useCallingPartyPhoneMask>' +
      '<callingPartyPrefixDigits>?</callingPartyPrefixDigits>' +
      '<digitDiscardInstructionName uuid="?">?</digitDiscardInstructionName>' +
      '<patternUrgency>?</patternUrgency>' +
      '<prefixDigitsOut>?</prefixDigitsOut>' +
      '<provideOutsideDialtone>?</provideOutsideDialtone>' +
      '<callingPartyNumberingPlan>?</callingPartyNumberingPlan>' +
      '<callingPartyNumberType>?</callingPartyNumberType>' +
      '<calledPartyNumberingPlan>?</calledPartyNumberingPlan>' +
      '<calledPartyNumberType>?</calledPartyNumberType>' +
      '<callingSearchSpaceName uuid="?">?</callingSearchSpaceName>' +
      '</returnedTags>' +
      '</ns:getTransPattern>'
    )
  );

  const trimJSON = json => this.trimJson(
    json,
    'getTransPattern',
    'transPattern'
  )

  // TODO: Error handling at this level

  return Promise.all([
    getSoapBody(query),
    this.constructHeaders('getTransPattern')
  ])
  .then(this.callApi)
  .then(this.parseResult)
  .then(this.convertXml)
  .then(trimJSON);
}
