/**
*
*
* @param {any} pattern
* @returns {Promise.<Response>} Response promise
*
*/
function listRoutePlan(pattern) {
  const getSoapBody = pattern => new Buffer(
    this.getSoapEnv().replace(
      '{{body}}',
      '<ns:listRoutePlan sequence="?">' +
      '<searchCriteria>' +
      '<dnOrPattern>' + '%' + pattern + '</dnOrPattern>' +
      '</searchCriteria>' +
      '<returnedTags uuid="?">' +
      '<dnOrPattern>?</dnOrPattern>' +
      '<partition uuid="?">?</partition>' +
      '<type>?</type>' +
      '<routeDetail>?</routeDetail>' +
      '</returnedTags>' +
      '</ns:listRoutePlan>'
    )
  )

  const trimJSON = json => this.trimJson(
    json, 'listRoutePlan', 'routePlan'
  )

  return Promise.all([
    getSoapBody(pattern),
    this.constructHeaders('listRoutePlan')
  ])
  .then(this.callApi)
  .then(this.parseResult)
  .then(this.convertXml)
  .then(trimJSON)
}

module.exports = listRoutePlan
