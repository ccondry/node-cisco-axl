'use strict';

const parser = require('xml2json');
const Axios = require('axios');

/**
 *
 *
 * @class AXL
 *
 * @constructor
 * @param {Object.<Options>} options
 * @property {Object.<Options>} options
 *
 * @example
 *
 * const AXL = require('node-cisco-axl');
 *
 * var axlOptions = {
 * host: process.env.CUCM,
 * user: process.env.AXLUSER,
 * pass: process.env.AXLPASS,
 * version: process.env.AXLVERSION
 * }
 *
 * const axl = new AXL(axlOptions);
 *
 * axl.listRoutePlan('9109200040')
 *    .then(uuid =>{
 *    console.log('uuid: '+uuid);
 * });
 *
 *
 *
 */


class AXL {
  constructor({
    host,
    user,
    pass,
    version
  }) {
    this.host = host || '';
    this.user = user || '';
    this.version = version || '';
    this.authToken = new Buffer(user + ':' + pass).toString('base64');
    this.axios = Axios.create({
      baseURL: 'https://' + host + ':8443/axl/',
      timeout: 8000
    });
    this.soapEnv = 'http://schemas.xmlsoap.org/soap/envelope/';
    this.soapNs = `http://www.cisco.com/AXL/API/${this.version}`;

    this.getSoapEnv = this.getSoapEnv.bind(this);
    this.constructHeaders = this.constructHeaders.bind(this);
    this.callApi = this.callApi.bind(this);

    this.getLine = require('./get-line').bind(this)
    this.getPhone = require('./get-phone').bind(this)
    this.getTranslationPattern = require('./get-translation-pattern').bind(this)
    this.listLdapDirectory = require('./list-ldap-directory').bind(this)
    this.listRoutePlan = require('./list-route-plan').bind(this)
    this.listTranslationPattern = require('./list-translation-pattern').bind(this)
    this.updatePhone = require('./update-phone').bind(this)
    this.updateLine = require('./update-line').bind(this)
    this.doLdapSync = require('./do-ldap-sync').bind(this)
    this.updateUserPin = require('./update-user-pin').bind(this)
    this.addPhone = require('./add-phone').bind(this)
  }

  getSoapEnv() {
    return (
      `<soapenv:Envelope xmlns:soapenv="${this.soapEnv}" xmlns:ns="${this.soapNs}">` +
      '<soapenv:Header/>' +
      '<soapenv:Body>' +
      '{{body}}' +
      '</soapenv:Body>' +
      '</soapenv:Envelope>'
    );
  }

  constructHeaders(func) {
    return {
      headers: {
        'SOAPAction': `CUCM:DB ver=${this.version} ${func}`,
        'Authorization': `Basic ${this.authToken}`,
        'Content-Type': 'text/xml; charset=utf-8',
      }
    };
  }

  callApi([body, headers]) {
    return this.axios.post('', body, headers);
  }

  parseResult(result) {
    return result.data;
  };

  convertXml(xml) {
    return parser.toJson(xml, {
      trim: true, object: true, sanitize: true
    });
  }

  trimJson(json, func, item) {
    const ret = trimJsonBody(json)['ns:' + func + 'Response']['return']
    if (item) {
      return ret[item]
    } else {
      return ret
    }
  }

  trimJsonBody(json) {
    return json['soapenv:Envelope']['soapenv:Body']
  }
}

module.exports = AXL;
