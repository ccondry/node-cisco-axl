/**
*
*
* @param {any} user
* @param {any} pin
* @returns {Promise.<Response>} Response promise
*
* @memberOf AXL
*/
module.exports = function (user, pin) {
  const getSoapBody = (user, pin) => new Buffer(
    this.getSoapEnv().replace(
      '{{body}}',
      '<ns:updateUser sequence="?">' +
      '<userid>' + user + '</userid>' +
      '<pin>' + pin + '</pin>' +
      '<pinCredentials>' +
      '<pinResetHackCount>t</pinResetHackCount>' +
      '</pinCredentials>' +
      '</ns:updateUser>'
    )
  );

  // TODO: Error handling at this level

  return Promise.all([
    getSoapBody(user, pin),
    this.constructHeaders('updateUser')
  ])
  .then(this.callApi)
  .then(this.parseResult)
  .then(this.convertXml)
  .then(this.trimJsonBody);
}
