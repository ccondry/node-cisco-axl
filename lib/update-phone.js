/**
*
*
* @param {any} name
* @returns {Promise.<Response>} Response promise
*
*
*/
module.exports = function ({name, uuid}) {
  const getSoapBody = pattern => {
    let id
    if (name) {
      id = '<name>' + name + '</name>'
    } else {
      id = '<uuid>' + uuid + '</uuid>'
    }
    return this.getSoapEnv().replace(
      '{{body}}',
      '<ns:updatePhone sequence="?">' +
      id +
      '<newName>?</newName>' +
      '<description>?</description>' +
      '<callingSearchSpaceName uuid="?">?</callingSearchSpaceName>' +
      '<devicePoolName uuid="?">?</devicePoolName>' +
      '<commonDeviceConfigName uuid="?">?</commonDeviceConfigName>' +
      '<commonPhoneConfigName uuid="?">?</commonPhoneConfigName>' +
      '<networkLocation>Use System Default</networkLocation>' +
      '<locationName uuid="?">?</locationName>' +
      '<mediaResourceListName uuid="?">?</mediaResouyrceListName>' +
      '<networkHoldMohAudioSourceId>?</networkHoldMohAudioSourceId>' +
      '<userHoldMohAudioSourceId>?</userHoldMohAudioSourceId>' +
      '<loadInformation special="?">?</loadInformation>' +
      '<vendorConfig>' +
      '</vendorConfig>' +
      '<securityProfileName uuid="?">?</securityProfileName>' +
      '<sipProfileName uuid="?">?</sipProfileName>' +
      '<removeLines>' +
      '<line ctiid="?">' +
      '<index>?</index>' +
      '<label>?</label>' +
      '<display>?</display>' +
      '<dirn uuid="?">' +
      '<pattern>?</pattern>' +
      '<routePartitionName uuid="?">?</routePartitionName>' +
      '</dirn>' +
      '<ringSetting>Ring</ringSetting>' +
      '<consecutiveRingSetting>Use System Default</consecutiveRingSetting>' +
      '<ringSettingIdlePickupAlert>Use System Default</ringSettingIdlePickupAlert>' +
      '<ringSettingActivePickupAlert>Use System Default</ringSettingActivePickupAlert>' +
      '<displayAscii>?</displayAscii>' +
      '<e164Mask>?</e164Mask>' +
      '<mwlPolicy>Use System Policy</mwlPolicy>' +
      '<maxNumCalls>2</maxNumCalls>' +
      '<busyTrigger>1</busyTrigger>' +
      '<callInfoDisplay>' +
      '<callerName>true</callerName>' +
      '<callerNumber>false</callerNumber>' +
      '<redirectedNumber>false</redirectedNumber>' +
      '<dialedNumber>true</dialedNumber>' +
      '</callInfoDisplay>' +
      '<recordingProfileName uuid="?">?</recordingProfileName>' +
      '<monitoringCssName uuid="?">?</monitoringCssName>' +
      '<recordingFlag>Call Recording Disabled</recordingFlag>' +
      '<audibleMwi>Default</audibleMwi>' +
      '<speedDial>?</speedDial>' +
      '<partitionUsage>General</partitionUsage>' +
      '<associatedEndusers>' +
      '<enduser>' +
      '<userId>?</userId>' +
      '</enduser>' +
      '</associatedEndusers>' +
      '<missedCallLogging>true</missedCallLogging>' +
      '<recordingMediaSource>Gateway Preferred</recordingMediaSource>' +
      '</line>' +
      '<lineIdentifier>' +
      '<directoryNumber>?</directoryNumber>' +
      '<routePartitionName>?</routePartitionName>' +
      '</lineIdentifier>' +
      '</removeLines>' +
      '<addLines>' +
      '<line ctiid="?">' +
      '<index>?</index>' +
      '<label>?</label>' +
      '<display>?</display>' +
      '<dirn uuid="?">' +
      '<pattern>?</pattern>' +
      '<routePartitionName uuid="?">?</routePartitionName>' +
      '</dirn>' +
      '<ringSetting>Ring</ringSetting>' +
      '<consecutiveRingSetting>Use System Default</consecutiveRingSetting>' +
      '<ringSettingIdlePickupAlert>Use System Default</ringSettingIdlePickupAlert>' +
      '<ringSettingActivePickupAlert>Use System Default</ringSettingActivePickupAlert>' +
      '<displayAscii>?</displayAscii>' +
      '<e164Mask>?</e164Mask>' +
      '<mwlPolicy>Use System Policy</mwlPolicy>' +
      '<maxNumCalls>2</maxNumCalls>' +
      '<busyTrigger>1</busyTrigger>' +
      '<callInfoDisplay>' +
      '<callerName>true</callerName>' +
      '<callerNumber>false</callerNumber>' +
      '<redirectedNumber>false</redirectedNumber>' +
      '<dialedNumber>true</dialedNumber>' +
      '</callInfoDisplay>' +
      '<recordingProfileName uuid="?">?</recordingProfileName>' +
      '<monitoringCssName uuid="?">?</monitoringCssName>' +
      '<recordingFlag>Call Recording Disabled</recordingFlag>' +
      '<audibleMwi>Default</audibleMwi>' +
      '<speedDial>?</speedDial>' +
      '<partitionUsage>General</partitionUsage>' +
      '<associatedEndusers>' +
      '<enduser>' +
      '<userId>?</userId>' +
      '</enduser>' +
      '</associatedEndusers>' +
      '<missedCallLogging>true</missedCallLogging>' +
      '<recordingMediaSource>Gateway Preferred</recordingMediaSource>' +
      '</line>' +
      '<lineIdentifier>' +
      '<directoryNumber>?</directoryNumber>' +
      '<routePartitionName>?</routePartitionName>' +
      '</lineIdentifier>' +
      '</addLines>' +
      '<lines>' +
      '<line ctiid="?">' +
      '<index>?</index>' +
      '<label>?</label>' +
      '<display>?</display>' +
      '<dirn uuid="?">' +
      '<pattern>?</pattern>' +
      '<routePartitionName uuid="?">?</routePartitionName>' +
      '</dirn>' +
      '<ringSetting>Ring</ringSetting>' +
      '<consecutiveRingSetting>Use System Default</consecutiveRingSetting>' +
      '<ringSettingIdlePickupAlert>Use System Default</ringSettingIdlePickupAlert>' +
      '<ringSettingActivePickupAlert>Use System Default</ringSettingActivePickupAlert>' +
      '<displayAscii>?</displayAscii>' +
      '<e164Mask>?</e164Mask>' +
      '<mwlPolicy>Use System Policy</mwlPolicy>' +
      '<maxNumCalls>2</maxNumCalls>' +
      '<busyTrigger>1</busyTrigger>' +
      '<callInfoDisplay>' +
      '<callerName>true</callerName>' +
      '<callerNumber>false</callerNumber>' +
      '<redirectedNumber>false</redirectedNumber>' +
      '<dialedNumber>true</dialedNumber>' +
      '</callInfoDisplay>' +
      '<recordingProfileName uuid="?">?</recordingProfileName>' +
      '<monitoringCssName uuid="?">?</monitoringCssName>' +
      '<recordingFlag>Call Recording Disabled</recordingFlag>' +
      '<audibleMwi>Default</audibleMwi>' +
      '<speedDial>?</speedDial>' +
      '<partitionUsage>General</partitionUsage>' +
      '<associatedEndusers>' +
      '<enduser>' +
      '<userId>?</userId>' +
      '</enduser>' +
      '</associatedEndusers>' +
      '<missedCallLogging>true</missedCallLogging>' +
      '<recordingMediaSource>Gateway Preferred</recordingMediaSource>' +
      '</line>' +
      '<lineIdentifier>' +
      '<directoryNumber>?</directoryNumber>' +
      '<routePartitionName>?</routePartitionName>' +
      '</lineIdentifier>' +
      '</lines>' +
      '<phoneTemplateName uuid="?">?</phoneTemplateName>' +
      '<speeddials>' +
      '<speeddial>' +
      '<dirn>?</dirn>' +
      '<label>?</label>' +
      '<index>?</index>' +
      '</speeddial>' +
      '</speeddials>' +
      '<busyLampFields>' +
      '<busyLampField>' +
      '<blfDest>?</blfDest>' +
      '<blfDirn>?</blfDirn>' +
      '<routePartition>?</routePartition>' +
      '<label>?</label>' +
      '<associatedBlfSdFeatures>' +
      '<feature>?</feature>' +
      '</associatedBlfSdFeatures>' +
      '<index>?</index>' +
      '</busyLampField>' +
      '</busyLampFields>' +
      '<primaryPhoneName uuid="?">?</primaryPhoneName>' +
      '<ringSettingIdleBlfAudibleAlert>Default</ringSettingIdleBlfAudibleAlert>' +
      '<ringSettingBusyBlfAudibleAlert>Default</ringSettingBusyBlfAudibleAlert>' +
      '<blfDirectedCallParks>' +
      '<blfDirectedCallPark>' +
      '<label>?</label>' +
      '<directedCallParkId>?</directedCallParkId>' +
      '<directedCallParkDnAndPartition>' +
      '<dnPattern>?</dnPattern>' +
      '<routePartitionName uuid="?">?</routePartitionName>' +
      '</directedCallParkDnAndPartition>' +
      '<index>?</index>' +
      '</blfDirectedCallPark>' +
      '</blfDirectedCallParks>' +
      '<addOnModules>' +
      '<addOnModule>' +
      '<loadInformation special="?">?</loadInformation>' +
      '<model>7914 14-Button Line Expansion Module</model>' +
      '<index>?</index>' +
      '</addOnModule>' +
      '</addOnModules>' +
      '<userLocale>?</userLocale>' +
      '<networkLocale>?</networkLocale>' +
      '<idleTimeout>?</idleTimeout>' +
      '<authenticationUrl>?</authenticationUrl>' +
      '<directoryUrl>?</directoryUrl>' +
      '<idleUrl>?</idleUrl>' +
      '<informationUrl>?</informationUrl>' +
      '<messagesUrl>?</messagesUrl>' +
      '<proxyServerUrl>?</proxyServerUrl>' +
      '<servicesUrl>?</servicesUrl>' +
      '<services>' +
      '<service>' +
      '<telecasterServiceName uuid="?">?</telecasterServiceName>' +
      '<name>?</name>' +
      '<url>?</url>' +
      '<urlButtonIndex>0</urlButtonIndex>' +
      '<urlLabel>?</urlLabel>' +
      '<serviceNameAscii>?</serviceNameAscii>' +
      '</service>' +
      '</services>' +
      '<softkeyTemplateName uuid="?">?</softkeyTemplateName>' +
      '<defaultProfileName uuid="?">?</defaultProfileName>' +
      '<enableExtensionMobility>?</enableExtensionMobility>' +
      '<singleButtonBarge>Default</singleButtonBarge>' +
      '<joinAcrossLines>Default</joinAcrossLines>' +
      '<builtInBridgeStatus>Default</builtInBridgeStatus>' +
      '<callInfoPrivacyStatus>Default</callInfoPrivacyStatus>' +
      '<hlogStatus>?</hlogStatus>' +
      '<ownerUserName uuid="?">?</ownerUserName>' +
      '<ignorePresentationIndicators>false</ignorePresentationIndicators>' +
      '<packetCaptureMode>None</packetCaptureMode>' +
      '<packetCaptureDuration>0</packetCaptureDuration>' +
      '<subscribeCallingSearchSpaceName uuid="?">?</subscribeCallingSearchSpaceName>' +
      '<rerouteCallingSearchSpaceName uuid="?">?</rerouteCallingSearchSpaceName>' +
      '<allowCtiControlFlag>?</allowCtiControlFlag>' +
      '<presenceGroupName uuid="?">?</presenceGroupName>' +
      '<unattendedPort>false</unattendedPort>' +
      '<requireDtmfReception>false</requireDtmfReception>' +
      '<rfc2833Disabled>false</rfc2833Disabled>' +
      '<certificateOperation>No Pending Operation</certificateOperation>' +
      '<authenticationMode>By Null String</authenticationMode>' +
      '<keySize>1024</keySize>' +
      '<keyOrder>RSA Only</keyOrder>' +
      '<ecKeySize>384</ecKeySize>' +
      '<authenticationString>?</authenticationString>' +
      '<upgradeFinishTime>?</upgradeFinishTime>' +
      '<deviceMobilityMode>Default</deviceMobilityMode>' +
      '<remoteDevice>false</remoteDevice>' +
      '<dndOption>Ringer Off</dndOption>' +
      '<dndRingSetting>?</dndRingSetting>' +
      '<dndStatus>?</dndStatus>' +
      '<isActive>true</isActive>' +
      '<mobilityUserIdName uuid="?">?</mobilityUserIdName>' +
      '<phoneSuite>Default</phoneSuite>' +
      '<phoneServiceDisplay>Default</phoneServiceDisplay>' +
      '<isProtected>false</isProtected>' +
      '<mtpRequired>?</mtpRequired>' +
      '<mtpPreferedCodec>711ulaw</mtpPreferedCodec>' +
      '<dialRulesName uuid="?">?</dialRulesName>' +
      '<sshUserId>?</sshUserId>' +
      '<sshPwd>?</sshPwd>' +
      '<digestUser>?</digestUser>' +
      '<outboundCallRollover>No Rollover</outboundCallRollover>' +
      '<hotlineDevice>false</hotlineDevice>' +
      '<secureInformationUrl>?</secureInformationUrl>' +
      '<secureDirectoryUrl>?</secureDirectoryUrl>' +
      '<secureMessageUrl>?</secureMessageUrl>' +
      '<secureServicesUrl>?</secureServicesUrl>' +
      '<secureAuthenticationUrl>?</secureAuthenticationUrl>' +
      '<secureIdleUrl>?</secureIdleUrl>' +
      '<alwaysUsePrimeLine>Default</alwaysUsePrimeLine>' +
      '<alwaysUsePrimeLineForVoiceMessage>Default</alwaysUsePrimeLineForVoiceMessage>' +
      '<featureControlPolicy uuid="?">?</featureControlPolicy>' +
      '<deviceTrustMode>Not Trusted</deviceTrustMode>' +
      '<earlyOfferSupportForVoiceCall>false</earlyOfferSupportForVoiceCall>' +
      '<requireThirdPartyRegistration>?</requireThirdPartyRegistration>' +
      '<blockIncomingCallsWhenRoaming>?</blockIncomingCallsWhenRoaming>' +
      '<homeNetworkId>?</homeNetworkId>' +
      '<AllowPresentationSharingUsingBfcp>false</AllowPresentationSharingUsingBfcp>' +
      '<confidentialAccess>' +
      '<confidentialAccessMode>?</confidentialAccessMode>' +
      '<confidentialAccessLevel>?</confidentialAccessLevel>' +
      '</confidentialAccess>' +
      '<requireOffPremiseLocation>false</requireOffPremiseLocation>' +
      '<allowiXApplicableMedia>false</allowiXApplicableMedia>' +
      '<cgpnIngressDN uuid="?">?</cgpnIngressDN>' +
      '<useDevicePoolCgpnIngressDN>true</useDevicePoolCgpnIngressDN>' +
      '<msisdn>?</msisdn>' +
      '<enableCallRoutingToRdWhenNoneIsActive>false</enableCallRoutingToRdWhenNoneIsActive>' +
      '<wifiHotspotProfile uuid="?">?</wifiHotspotProfile>' +
      '<wirelessLanProfileGroup uuid="?">?</wirelessLanProfileGroup>' +
      '<elinGroup uuid="?">?</elinGroup>' +
      '</ns:updatePhone>' +
    )
  }

  // TODO: Error handling at this level

  return Promise.all([
    getSoapBody(pattern),
    this.constructHeaders('updatePhone')
  ])
  .then(callApi)
  .then(parseResult)
  .then(xmltoJSON)
  .then(this.trimJsonBody);
}
