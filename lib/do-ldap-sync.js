/**
*
*
* @param {any} uuid
* @returns {Promise.<Response>} Response promise
*
* @memberOf AXL
*/
module.exports = function (uuid) {
  const getSoapBody = uuid => new Buffer(
    this.getSoapEnv().replace(
      '{{body}}',
      '<ns:doLdapSync sequence="?">' +
      '<uuid>' + uuid + '</uuid>' +
      '<sync>t</sync>' +
      '<returnedTags uuid="?">' +
      '</returnedTags>' +
      '</ns:doLdapSync>'
    )
  );

  const trimJSON = json => this.trimJson(
    json, 'doLdapSync'
  )

  // TODO: Error handling at this level

  return Promise.all([
    getSoapBody(uuid),
    this.constructHeaders('doLdapSync')
  ])
  .then(this.callApi)
  .then(this.parseResult)
  .then(this.convertXml)
  .then(trimJSON);
}
