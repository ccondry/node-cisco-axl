/**
 *
 *
 * @param {any} query
 * @returns {Promise.<Response>} Response promise
 *
 *
 */
module.exports = function (query) {
  const getSoapBody = (query) => new Buffer(
    this.getSoapEnv().replace(
      '{{body}}',
      '<ns:getPhone sequence="?">' +
      '<'+query.type+'>'+ query.search +'</'+query.type+'>' +
      '<returnedTags ctiid="?" uuid="?">' +
      '<name>?</name>' +
      '<description>?</description>' +
      '<product>?</product>' +
      '<model>?</model>' +
      '<class>?</class>' +
      '<protocol>?</protocol>' +
      '<callingSearchSpaceName uuid="?">?</callingSearchSpaceName>' +
      '<devicePoolName uuid="?">?</devicePoolName>' +
      '<commonDeviceConfigName uuid="?">?</commonDeviceConfigName>' +
      '<commonPhoneConfigName uuid="?">?</commonPhoneConfigName>' +
      '<networkLocation>?</networkLocation>' +
      '<locationName uuid="?">?</locationName>' +
      '<mediaResourceListName uuid="?">?</mediaResourceListName>' +
      '<networkHoldMohAudioSourceId>?</networkHoldMohAudioSourceId>' +
      '<userHoldMohAudioSourceId>?</userHoldMohAudioSourceId>' +
      '<securityProfileName uuid="?">?</securityProfileName>' +
      '<sipProfileName uuid="?">?</sipProfileName>' +
      '<geoLocationName uuid="?">?</geoLocationName>' +
      '<lines>' +
      '<line ctiid="?" uuid="?">' +
      '<index>?</index>' +
      '<label>?</label>' +
      '<display>?</display>' +
      '<dirn uuid="?">' +
      '<pattern>?</pattern>' +
      '<routePartitionName uuid="?">?</routePartitionName>' +
      '</dirn>' +
      '<ringSetting>?</ringSetting>' +
      '<consecutiveRingSetting>?</consecutiveRingSetting>' +
      '<displayAscii>?</displayAscii>' +
      '<e164Mask>?</e164Mask>' +
      '<mwlPolicy>?</mwlPolicy>' +
      '<maxNumCalls>?</maxNumCalls>' +
      '<busyTrigger>?</busyTrigger>' +
      '<callInfoDisplay>' +
      '<callerName>?</callerName>' +
      '<callerNumber>?</callerNumber>' +
      '<redirectedNumber>?</redirectedNumber>' +
      '<dialedNumber>?</dialedNumber>' +
      '</callInfoDisplay>' +
      '<associatedEndusers>' +
      '<enduser>' +
      '<userId>?</userId>' +
      '</enduser>' +
      '</associatedEndusers>' +
      '</line>' +
      '<lineIdentifier>' +
      '<directoryNumber>?</directoryNumber>' +
      '<routePartitionName>?</routePartitionName>' +
      '</lineIdentifier>' +
      '</lines>' +
      '<numberOfButtons>?</numberOfButtons>' +
      '<phoneTemplateName uuid="?">?</phoneTemplateName>' +
      '<speeddials>' +
      '<speeddial>' +
      '<dirn>?</dirn>' +
      '<label>?</label>' +
      '<index>?</index>' +
      '</speeddial>' +
      '</speeddials>' +
      '<busyLampFields>' +
      '<busyLampField>' +
      '<blfDest>?</blfDest>' +
      '<label>?</label>' +
      '<index>?</index>' +
      '</busyLampField>' +
      '</busyLampFields>' +
      '<userLocale>?</userLocale>' +
      '<networkLocale>?</networkLocale>' +
      '<idleTimeout>?</idleTimeout>' +
      '<authenticationUrl>?</authenticationUrl>' +
      '<directoryUrl>?</directoryUrl>' +
      '<idleUrl>?</idleUrl>' +
      '<informationUrl>?</informationUrl>' +
      '<messagesUrl>?</messagesUrl>' +
      '<proxyServerUrl>?</proxyServerUrl>' +
      '<servicesUrl>?</servicesUrl>' +
      '<services>' +
      '<service uuid="?">' +
      '<telecasterServiceName uuid="?">?</telecasterServiceName>' +
      '<name>?</name>' +
      '<url>?</url>' +
      '<urlButtonIndex>?</urlButtonIndex>' +
      '<urlLabel>?</urlLabel>' +
      '<serviceNameAscii>?</serviceNameAscii>' +
      '<phoneService>?</phoneService>' +
      '</service>' +
      '</services>' +
      '<softkeyTemplateName uuid="?">?</softkeyTemplateName>' +
      '<loginUserId>?</loginUserId>' +
      '<defaultProfileName uuid="?">?</defaultProfileName>' +
      '<enableExtensionMobility>?</enableExtensionMobility>' +
      '<currentProfileName uuid="?">?</currentProfileName>' +
      '<loginTime>?</loginTime>' +
      '<loginDuration>?</loginDuration>' +
      '<currentConfig>' +
      '<userHoldMohAudioSourceId>?</userHoldMohAudioSourceId>' +
      '<phoneTemplateName uuid="?">?</phoneTemplateName>' +
      '<mlppDomainId>?</mlppDomainId>' +
      '<mlppIndicationStatus>?</mlppIndicationStatus>' +
      '<preemption>?</preemption>' +
      '<softkeyTemplateName uuid="?">?</softkeyTemplateName>' +
      '<ignorePresentationIndicators>?</ignorePresentationIndicators>' +
      '<singleButtonBarge>?</singleButtonBarge>' +
      '<joinAcrossLines>?</joinAcrossLines>' +
      '<callInfoPrivacyStatus>?</callInfoPrivacyStatus>' +
      '<dndStatus>?</dndStatus>' +
      '<dndRingSetting>?</dndRingSetting>' +
      '<dndOption>?</dndOption>' +
      '<alwaysUsePrimeLine>?</alwaysUsePrimeLine>' +
      '<alwaysUsePrimeLineForVoiceMessage>?</alwaysUsePrimeLineForVoiceMessage>' +
      '<emccCallingSearchSpaceName uuid="?">?</emccCallingSearchSpaceName>' +
      '<deviceName>?</deviceName>' +
      '<model>?</model>' +
      '<product>?</product>' +
      '<deviceProtocol>?</deviceProtocol>' +
      '<class>?</class>' +
      '<addressMode>?</addressMode>' +
      '<allowAutoConfig>?</allowAutoConfig>' +
      '<remoteSrstOption>?</remoteSrstOption>' +
      '<remoteSrstIp>?</remoteSrstIp>' +
      '<remoteSrstPort>?</remoteSrstPort>' +
      '<remoteSipSrstIp>?</remoteSipSrstIp>' +
      '<remoteSipSrstPort>?</remoteSipSrstPort>' +
      '<geolocationInfo>?</geolocationInfo>' +
      '<remoteLocationName>?</remoteLocationName>' +
      '</currentConfig>' +
      '<hlogStatus>?</hlogStatus>' +
      '<ownerUserName uuid="?">?</ownerUserName>' +
      '</returnedTags>' +
      '</ns:getPhone>'
    )
  )

  const trimJSON = json => this.trimJson(
    json,
    'getPhone',
    'phone'
  )

  return Promise.all([
    getSoapBody(query),
    this.constructHeaders('getPhone')
  ])
    .then(this.callApi)
    .then(this.parseResult)
    .then(this.convertXml)
    .then(trimJSON);
}
