/**
*
*
* @param {any} pattern
* @returns {Promise.<Response>} Response promise
*
*
*/
function listTransPattern(pattern) {
  const getSoapBody = pattern => new Buffer(
    this.getSoapEnv().replace(
      '{{body}}',
      '<ns:listTransPattern sequence="?">' +
      '<searchCriteria>' +
      '<pattern>%' + pattern + '</pattern>' +
      '</searchCriteria>' +
      '<returnedTags uuid="?">' +
      '<pattern>?</pattern>' +
      '<description>?</description>' +
      '<routePartitionName uuid="?">?</routePartitionName>' +
      '<calledPartyTransformationMask>?</calledPartyTransformationMask>' +
      '<callingPartyTransformationMask>?</callingPartyTransformationMask>' +
      '<useCallingPartyPhoneMask>?</useCallingPartyPhoneMask>' +
      '<callingPartyPrefixDigits>?</callingPartyPrefixDigits>' +
      '<digitDiscardInstructionName uuid="?">?</digitDiscardInstructionName>' +
      '<patternUrgency>?</patternUrgency>' +
      '<prefixDigitsOut>?</prefixDigitsOut>' +
      '<provideOutsideDialtone>?</provideOutsideDialtone>' +
      '<callingPartyNumberingPlan>?</callingPartyNumberingPlan>' +
      '<callingPartyNumberType>?</callingPartyNumberType>' +
      '<calledPartyNumberingPlan>?</calledPartyNumberingPlan>' +
      '<calledPartyNumberType>?</calledPartyNumberType>' +
      '<callingSearchSpaceName uuid="?">?</callingSearchSpaceName>' +
      '</returnedTags>' +
      '</ns:listTransPattern>'
    )
  )

  const trimJSON = json => this.trimJson(
    json,
    'listTransPattern',
    'transPattern'
  )

  return Promise.all([
    getSoapBody(pattern),
    this.constructHeaders('listTransPattern')
  ])
  .then(this.callApi)
  .then(this.parseResult)
  .then(this.convertXml)
  .then(trimJSON)
}

module.exports = listTransPattern
