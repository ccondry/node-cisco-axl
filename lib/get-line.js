/**
 *
 *
 * @param {any} query
 * @returns {Promise.<Response>} Response promise
 *
 *
 */
function getLine(query) {
  const getSoapBody = (query) => new Buffer(
    this.getSoapEnv().replace(
      '{{body}}',
      '<ns:getLine sequence="?">' +
      '<'+query.type+'>' + query.search + '</'+query.type+'>' +
      '<routePartitionName uuid="?">' + (query.partition || '') + '</routePartitionName>'+
      '<returnedTags uuid="?">' +
      '<pattern>?</pattern>' +
      '<description>?</description>' +
      '<usage>?</usage>' +
      '<routePartitionName uuid="?">?</routePartitionName>' +
      '<callForwardAll>' +
      '<forwardToVoiceMail>?</forwardToVoiceMail>' +
      '<callingSearchSpaceName uuid="?">?</callingSearchSpaceName>' +
      '<secondaryCallingSearchSpaceName uuid="?">?</secondaryCallingSearchSpaceName>' +
      '<destination>?</destination>' +
      '</callForwardAll>' +
      '<callForwardBusy>' +
      '<forwardToVoiceMail>?</forwardToVoiceMail>' +
      '<callingSearchSpaceName uuid="?">?</callingSearchSpaceName>' +
      '<destination>?</destination>' +
      '</callForwardBusy>' +
      '<callForwardBusyInt>' +
      '<forwardToVoiceMail>?</forwardToVoiceMail>' +
      '<callingSearchSpaceName uuid="?">?</callingSearchSpaceName>' +
      '<destination>?</destination>' +
      '</callForwardBusyInt>' +
      '<callForwardNoAnswer>' +
      '<forwardToVoiceMail>?</forwardToVoiceMail>' +
      '<callingSearchSpaceName uuid="?">?</callingSearchSpaceName>' +
      '<destination>?</destination>' +
      '<duration>?</duration>' +
      '</callForwardNoAnswer>' +
      '<callForwardNoAnswerInt>' +
      '<forwardToVoiceMail>?</forwardToVoiceMail>' +
      '<callingSearchSpaceName uuid="?">?</callingSearchSpaceName>' +
      '<destination>?</destination>' +
      '<duration>?</duration>' +
      '</callForwardNoAnswerInt>' +
      '<callForwardNoCoverage>' +
      '<forwardToVoiceMail>?</forwardToVoiceMail>' +
      '<callingSearchSpaceName uuid="?">?</callingSearchSpaceName>' +
      '<destination>?</destination>' +
      '</callForwardNoCoverage>' +
      '<callForwardNoCoverageInt>' +
      '<forwardToVoiceMail>?</forwardToVoiceMail>' +
      '<callingSearchSpaceName uuid="?">?</callingSearchSpaceName>' +
      '<destination>?</destination>' +
      '</callForwardNoCoverageInt>' +
      '<callForwardOnFailure>' +
      '<forwardToVoiceMail>?</forwardToVoiceMail>' +
      '<callingSearchSpaceName uuid="?">?</callingSearchSpaceName>' +
      '<destination>?</destination>' +
      '</callForwardOnFailure>' +
      '<callForwardAlternateParty>' +
      '<forwardToVoiceMail>?</forwardToVoiceMail>' +
      '<callingSearchSpaceName uuid="?">?</callingSearchSpaceName>' +
      '<destination>?</destination>' +
      '<duration>?</duration>' +
      '</callForwardAlternateParty>' +
      '<callForwardNotRegistered>' +
      '<forwardToVoiceMail>?</forwardToVoiceMail>' +
      '<callingSearchSpaceName uuid="?">?</callingSearchSpaceName>' +
      '<destination>?</destination>' +
      '</callForwardNotRegistered>' +
      '<callForwardNotRegisteredInt>' +
      '<forwardToVoiceMail>?</forwardToVoiceMail>' +
      '<callingSearchSpaceName uuid="?">?</callingSearchSpaceName>' +
      '<destination>?</destination>' +
      '</callForwardNotRegisteredInt>' +
      '<callPickupGroupName uuid="?">?</callPickupGroupName>' +
      '<autoAnswer>?</autoAnswer>' +
      '<networkHoldMohAudioSourceId>?</networkHoldMohAudioSourceId>' +
      '<userHoldMohAudioSourceId>?</userHoldMohAudioSourceId>' +
      '<alertingName>?</alertingName>' +
      '<asciiAlertingName>?</asciiAlertingName>' +
      '<presenceGroupName uuid="?">?</presenceGroupName>' +
      '<shareLineAppearanceCssName uuid="?">?</shareLineAppearanceCssName>' +
      '<voiceMailProfileName uuid="?">?</voiceMailProfileName>' +
      '<defaultActivatedDeviceName uuid="?">?</defaultActivatedDeviceName>' +
      '<directoryURIs>' +
      '<directoryUri uuid="?">' +
      '<isPrimary>?</isPrimary>' +
      '<uri>?</uri>' +
      '<partition uuid="?">?</partition>' +
      '<advertiseGloballyViaIls>?</advertiseGloballyViaIls>' +
      '</directoryUri>' +
      '</directoryURIs>' +
      '<allowCtiControlFlag>?</allowCtiControlFlag>' +
      '<associatedDevices>' +
      '<device>?</device>' +
      '</associatedDevices>' +
      '<active>?</active>' +
      '</returnedTags>' +
      '</ns:getLine>'
    )
  );

  const trimJSON = json => this.trimJson(
    json,
    'getLine',
    'line'
  )
  // TODO: Error handling at this level

  return Promise.all([
    getSoapBody(query),
    this.constructHeaders('getLine')
  ])
  .then(this.callApi)
  .then(this.parseResult)
  .then(this.convertXml)
  .then(trimJSON);
}

module.exports = getLine
